# EXT: scriptmerger

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/scriptmerger

Please report bugs here: https://gitlab.sgalinski.de/typo3/scriptmerger/-/issues

## About

The scriptmerger merges and minifies all JS and CSS files into a single one.
Files can be marked to be ignored by placing a data-ignore="1" attribute on their include tag.
