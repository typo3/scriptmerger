<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
	'scriptmerger',
	'Configuration/TypoScript/',
	'Scriptmerger'
);
